import {Component, Input, OnInit} from '@angular/core';
import {faTrashAlt} from '@fortawesome/pro-regular-svg-icons';
import {MatDialog} from '@angular/material/dialog';
@Component({
  selector: 'app-docs-uploader',
  templateUrl: './docs-uploader.component.html',
  styleUrls: ['./docs-uploader.component.scss']
})
export class DocsUploaderComponent implements OnInit {
  @Input() fileType = 'image';
  @Input() editMode = false;
  faTrashAlt = faTrashAlt;
  uploadingFile: any;
  uploadWarningFile: any;
  uploadedFile: any;
  fileAttachedResp: any;
  uploadFailedFile: any;
  fileProgressNumber: any;
  fileExtention: string;
  fileObj: any;

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  uploadFile() {
  }

  handleFileInput(files: any) {

  }

  removeDoc() {

  }

  downloadDoc(attachmentId: any) {

  }
}
