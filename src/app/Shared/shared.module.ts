import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FloatButtonComponent} from './float-button/float-button.component';

import {MaterialModule} from '../material.module';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {DocsUploaderComponent} from './doc-uploader/docs-uploader.component';


@NgModule({
  declarations: [
    DocsUploaderComponent,
    FloatButtonComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FontAwesomeModule
  ],
  exports: [
    MaterialModule,
    DocsUploaderComponent,
    FloatButtonComponent,
    FontAwesomeModule
  ]
})
export class SharedModule {
}
