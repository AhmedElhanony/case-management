import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-docs-uploader',
  templateUrl: './docs-uploader.component.html',
  styleUrls: ['./docs-uploader.component.scss']
})
export class DocsUploaderComponent implements OnInit {
  uploadingFile: any;
  uploadWarningFile: any;
  uploadedFile: any;
  fileAttachedResp: any;
  uploadFailedFile: any;
  fileProgressNumber: any;
  fileExtention: string;
  fileObj: any;

  constructor() { }

  ngOnInit(): void {
  }

  uploadFile() {

  }

  handleFileInput(files: any) {

  }

  removeDoc() {

  }

  downloadDoc(attachmentId: any) {

  }
}
