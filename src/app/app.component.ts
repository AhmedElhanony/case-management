import {Component} from '@angular/core';
import {Directionality} from '@angular/cdk/bidi';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  private isRTL: boolean;
  private dirChangeSubscription = Subscription.EMPTY;

  constructor(dir: Directionality) {
    this.isRTL = dir.value === 'rtl';

    this.dirChangeSubscription = dir.change.subscribe((value) => {
      console.log('dir changed', value);
    });

    dir.change.emit('rtl');
  }
}
