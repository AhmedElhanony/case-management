import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SandBoxRoutingModule} from './sandbox-routing.module';
import {SandboxComponent} from './sandbox.component';
import {CasesComponent} from './cases/cases.component';
import {CasesFiltersComponent} from './cases/cases-filters/cases-filters.component';
import {AddCasePopupComponent} from './cases/add-case-popup/add-case-popup.component';
import {DashboardComponent} from './dashboard/dashboard.component';

import {HighchartsChartModule} from 'highcharts-angular';
import {UpcomingeventsComponent} from './dashboard/upcoming-events/upcoming-events.component';
import {RecentactivityComponent} from './dashboard/recent-activity/recent-activity.component';
import {DashboardFiltersComponent} from './dashboard/dashboard-filters/dashboard-filters.component';
import {CasesDialogComponent} from './dashboard/cases-dialog/cases-dialog.component';
import {AddEventComponent} from './dashboard/upcoming-events/add-event/add-event.component';
// import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../Shared/shared.module';
import {ContentComponent} from './content/content.component';
import {FiltersComponent} from './content/filters/filters.component';
import {AddDocumentsComponent} from './content/add-documents/add-documents.component';
import {CaseDocumentsComponent} from './content/case-documents/case-documents.component';
import {SatPopoverModule} from '@ncstate/sat-popover';
import {CaseDetailsComponent} from './cases/case-details/case-details.component';
import {CaseStagesComponent} from './cases/case-details/case-stages/case-stages.component';
import {CaseNotePopupComponent} from './cases/case-details/case-note-popup/case-note-popup.component';
import {CaseRecentActivityComponent} from './cases/case-details/case-recent-activity/case-recent-activity.component';
import {CloseCasePopupComponent} from './cases/case-details/close-case-popup/close-case-popup.component';
import {SendLawOfficeComponent} from './cases/case-details/send-law-office/send-law-office.component';
import {LitigantsComponent} from './litigants/litigants.component';
import {LawOfficeRepliesComponent} from './cases/case-details/law-office-replies/law-office-replies.component';
import {AddEditLitigantComponent} from './cases/add-edit-litigant/add-edit-litigant.component';
import {EditLitigantPopupComponent} from './cases/case-details/edit-litigant-popup/edit-litigant-popup.component';
import {DelegateTeamMemberComponent} from './cases/case-details/delegate-team-member/delegate-team-member.component';
import {OcrComponent} from './cases/ocr/ocr.component';
import { CalendarPageComponent } from './calendar-page/calendar-page.component';
import {FullCalendarModule} from '@fullcalendar/angular';
import {CalendarFilterComponent} from './calendar-page/calendar-filter/calendar-filter.component';
import { PersonsComponent } from './litigants/persons/persons.component';
import { CompaniesComponent } from './litigants/companies/companies.component';
import { GroupsComponent } from './litigants/groups/groups.component';
import { AddLitigantsComponent } from './litigants/add-litigants/add-litigants.component';
import { AddGroupComponent } from './litigants/groups/add-group/add-group.component';
import {SettingsComponent} from './settings/settings.component';
import {ImportComponent} from './settings/import/import.component';
import {ExportComponent} from './settings/export/export.component';
import { FeedBackComponent } from './cases/feed-back/feed-back.component';
import { CompleteCaseInfoComponent } from './cases/complete-case-info/complete-case-info.component';
import { ViewCasePopupComponent } from './cases/view-case-popup/view-case-popup.component';
import { AddCurrentEventComponent } from './cases/case-details/add-current-event/add-current-event.component';
import { AddNextEventComponent } from './cases/case-details/add-next-event/add-next-event.component';
import { InitialResultComponent } from './cases/case-details/initial-result/initial-result.component';
import { FinalResultComponent } from './cases/case-details/final-result/final-result.component';
import { ResultsDetailsComponent } from './cases/case-details/results-details/results-details.component';


@NgModule({
  declarations: [
    SandboxComponent,
    CasesComponent,
    CasesFiltersComponent,
    AddCasePopupComponent,
    DashboardComponent,
    UpcomingeventsComponent,
    RecentactivityComponent,
    DashboardFiltersComponent,
    CasesDialogComponent,
    AddEventComponent,
    SettingsComponent,
    ImportComponent,
    ExportComponent,
    ContentComponent,
    FiltersComponent,
    AddDocumentsComponent,
    CaseDocumentsComponent,
    CalendarFilterComponent,
    CaseDetailsComponent,
    CaseStagesComponent,
    CaseNotePopupComponent,
    CaseRecentActivityComponent,
    CloseCasePopupComponent,
    SendLawOfficeComponent,
    LitigantsComponent,
    LawOfficeRepliesComponent,
    AddEditLitigantComponent,
    EditLitigantPopupComponent,
    DelegateTeamMemberComponent,
    OcrComponent,
    CalendarPageComponent,
    PersonsComponent,
    CompaniesComponent,
    GroupsComponent,
    AddLitigantsComponent,
    AddGroupComponent,
    FeedBackComponent,
    CompleteCaseInfoComponent,
    ViewCasePopupComponent,
    AddCurrentEventComponent,
    AddNextEventComponent,
    InitialResultComponent,
    FinalResultComponent,
    ResultsDetailsComponent
  ],
  imports: [
    CommonModule,
    SandBoxRoutingModule,
    SharedModule,
    HighchartsChartModule,
    FormsModule,
    ReactiveFormsModule,
    // BsDatepickerModule.forRoot(),
    // RouterModule.forChild(routes),
    FullCalendarModule,
    SatPopoverModule
  ],
})
export class SandboxModule {
}
