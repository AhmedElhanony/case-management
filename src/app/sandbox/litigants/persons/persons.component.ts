import { Component, OnInit } from '@angular/core';
import {faSearch, faPlus, faMinus} from '@fortawesome/pro-regular-svg-icons';
@Component({
  selector: 'app-persons',
  templateUrl: './persons.component.html',
  styleUrls: ['./persons.component.scss']
})
export class PersonsComponent implements OnInit {
  faSearch = faSearch;
  faPlus = faPlus;
  faMinus = faMinus;
  contentDataSource = CONTENT_ELEMENT_DATA;
  displayedContentColumns = ['Name', 'GroupName', 'Contact', 'Actions'];
  constructor() { }

  ngOnInit(): void {
  }

}


const CONTENT_ELEMENT_DATA = [
  {
    name: 'Ux team',
    GroupName: 'Telecommunications',
    Contact: 'ahmedelhanony@gmail.com'
  },
  {
    name: 'Ahmed abbasy',
    GroupName: 'Telecommunications',
    Contact: 'ahmedelhanony@gmail.com'
  },
  {
    name: 'Ahmed abbasy',
    GroupName: 'Telecommunications',
    Contact: 'ahmedelhanony@gmail.com'
  },

];
