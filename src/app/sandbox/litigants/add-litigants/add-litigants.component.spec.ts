import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLitigantsComponent } from './add-litigants.component';

describe('AddLitigantsComponent', () => {
  let component: AddLitigantsComponent;
  let fixture: ComponentFixture<AddLitigantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLitigantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLitigantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
