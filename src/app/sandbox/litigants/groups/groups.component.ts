import { Component, OnInit } from '@angular/core';
import {faSearch, faPlus, faMinus} from '@fortawesome/pro-regular-svg-icons';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatDialog} from '@angular/material/dialog';
import {AddGroupComponent} from './add-group/add-group.component';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class GroupsComponent implements OnInit {
  faSearch = faSearch;
  faPlus = faPlus;
  faMinus = faMinus;
  dataSource = TABLE_DATA;

  columnsToDisplay = ['groupName', 'contactsNo', 'creationDate', 'actions'];

  expandedElement;
  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  openAddGroup() {
    const dialogRef = this.dialog.open(AddGroupComponent, {
      data: {},
      panelClass: 'main-popup',
      width: '750px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }



}

const TABLE_DATA = [
  {
    GroupName: 'individuals ',
    ContactsNo: '2',
    CreationDate: 'Today'
  }, {
    GroupName: 'individuals ',
    ContactsNo: '2',
    CreationDate: 'Today'
  }, {
    GroupName: 'individuals ',
    ContactsNo: '2',
    CreationDate: 'Today'
  },
];



