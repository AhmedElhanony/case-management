import {Component, OnInit} from '@angular/core';
import {faSearch, faPlus, faMinus} from '@fortawesome/pro-regular-svg-icons';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatDialog} from '@angular/material/dialog';
import {AddLitigantsComponent} from './add-litigants/add-litigants.component';

@Component({
  selector: 'app-litigants',
  templateUrl: './litigants.component.html',
  styleUrls: ['./litigants.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class LitigantsComponent implements OnInit {
  faSearch = faSearch;
  faPlus = faPlus;
  faMinus = faMinus;



  constructor(public dialog: MatDialog) {
  }

  ngOnInit(): void {
  }

  openAddLitigants() {
    const dialogRef = this.dialog.open(AddLitigantsComponent, {
      data: {},
      panelClass: 'main-popup',
      width: '750px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}


