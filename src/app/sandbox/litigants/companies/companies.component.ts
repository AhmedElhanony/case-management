import { Component, OnInit } from '@angular/core';
import {faSearch, faPlus, faMinus} from '@fortawesome/pro-regular-svg-icons';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class CompaniesComponent implements OnInit {
  faSearch = faSearch;
  faPlus = faPlus;
  faMinus = faMinus;
  expandedElement;
  companiesDataSource = COMPANIES_TABLE_DATA;
  companiesColumnsToDisplay = ['name', 'groupName', 'contact', 'actions'];
  constructor() { }

  ngOnInit(): void {
  }

}
const COMPANIES_TABLE_DATA = [
  {
    name: 'Company 1',
    GroupName: 'individuals ',
    Contact: '2'
  },
  {
    name: 'Company 1',
    GroupName: 'individuals ',
    Contact: '2'
  }, {
    name: 'Company 1',
    GroupName: 'individuals ',
    Contact: '2'
  }, {
    name: 'Company 1',
    GroupName: 'individuals ',
    Contact: '2'
  },
];
