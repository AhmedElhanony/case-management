import {Component, OnInit} from '@angular/core';
import {faSearch} from '@fortawesome/pro-regular-svg-icons';

@Component({
  selector: 'app-sandbox',
  templateUrl: './sandbox.component.html',
  styleUrls: ['./sandbox.component.scss']
})
export class SandboxComponent implements OnInit {
  faSearch = faSearch;

  constructor() {
  }

  ngOnInit(): void {
  }

}
