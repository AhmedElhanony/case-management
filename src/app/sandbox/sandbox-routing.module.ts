import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {CasesComponent} from './cases/cases.component';
import {CaseDetailsComponent} from './cases/case-details/case-details.component';
import {LitigantsComponent} from './litigants/litigants.component';
import { ContentComponent } from './content/content.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {CalendarPageComponent} from './calendar-page/calendar-page.component';
import {SettingsComponent} from './settings/settings.component';

const routes: Routes = [{
  path: '',
  // component: SandboxComponent,
  children: [
    {
      path: 'cases-list',
      component: CasesComponent
    },
    {
      path: 'case-details',
      component: CaseDetailsComponent
    },
    {
      path: 'litigants',
      component: LitigantsComponent
    },
    {
      path: 'content',
      component: ContentComponent
    },
    {
      path: 'calendar',
      component: CalendarPageComponent
    },
    {
      path: 'dashboard',
      component: DashboardComponent
    },
    { path: 'settings', component: SettingsComponent },
    {
      path: '',
      redirectTo: 'cases-list',
      pathMatch: 'full',
    }
  ],
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class SandBoxRoutingModule {
}
