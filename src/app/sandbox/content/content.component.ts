import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {AddDocumentsComponent} from './add-documents/add-documents.component';
import {CaseDocumentsComponent} from './case-documents/case-documents.component';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  openAddDoc() {
    const dialogRef = this.dialog.open(AddDocumentsComponent, {
      data: {},
      panelClass: 'mobile-dialog',
      width: '750px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }


  openCaseDocuments() {
    const dialogRef = this.dialog.open(CaseDocumentsComponent, {
      data: {},
      panelClass: 'mobile-dialog',
      width: '750px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  constructor(public dialog: MatDialog) {
  }

  ngOnInit(): void {
  }

}
