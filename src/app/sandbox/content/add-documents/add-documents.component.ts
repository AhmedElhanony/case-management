import { Component, OnInit } from '@angular/core';
import {faPlus, faTimes} from '@fortawesome/pro-light-svg-icons';

@Component({
  selector: 'app-add-documents',
  templateUrl: './add-documents.component.html',
  styleUrls: ['./add-documents.component.scss']
})
export class AddDocumentsComponent implements OnInit {
  faPlus = faPlus;
  faTimes = faTimes;
  constructor() { }

  ngOnInit(): void {
  }

}
