import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FullCalendarComponent} from '@fullcalendar/angular';
import {MatMenuTrigger} from '@angular/material/menu';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGrigPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import {EventInput} from '@fullcalendar/core';
import {AddEventComponent} from '../dashboard/upcoming-events/add-event/add-event.component';
import {MatDialog} from '@angular/material/dialog';
import {CalendarFilterComponent} from './calendar-filter/calendar-filter.component';
import { SatPopoverAnchor } from '@ncstate/sat-popover';

@Component({
  selector: 'app-calendar-page',
  templateUrl: './calendar-page.component.html',
  styleUrls: ['./calendar-page.component.scss']
})
export class CalendarPageComponent implements OnInit {
  @ViewChild('calendar', {static: false}) calendarComponent: FullCalendarComponent; // the #calendar in the template
  @ViewChild('popover') popover;
  anchor: SatPopoverAnchor | ElementRef<HTMLElement> | HTMLElement;
  @ViewChild(MatMenuTrigger) notificationMenuBtn: MatMenuTrigger;
  calendarPlugins = [dayGridPlugin, timeGrigPlugin, interactionPlugin];
  calendarWeekends = true;
  calendarEvents: EventInput[] = [
    {
      title: 'Data structure programming course',
      start: new Date('Sun Feb 8 2020 15:44:48 GMT+0300'),
      end: new Date('Sun Feb 8 2020 15:44:48 GMT+0300'),
      className: 'pending',
    },
    {
      title: 'User research and market research course ',
      start: new Date('Sun Feb 5 2020 15:44:48 GMT+0300'),
      end: new Date('Sun Feb 7 2020 15:44:48 GMT+0300'),
      className: 'confirmed',
    },
    {
      title: 'Data analysis fundamentals cou..',
      start: new Date(),
      end: new Date(),
      className: 'enrolled',
    }
  ];
  popoverPosition = {top: 0, left: 0};
  popoverClass: string;



  openFilters() {
    const dialogRef = this.dialog.open(CalendarFilterComponent, {
      data: {},
      panelClass: 'mobile-popup',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }


  openAddEvent() {
    const dialogRef = this.dialog.open(AddEventComponent, {
      data: {},
      panelClass: 'mobile-dialog',
      width: '60%'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  constructor(public dialog: MatDialog) {}

  ngOnInit() {
  }

  handleDateClick($event: any) {
    // if (confirm('Would you like to add an event to ' + $event.dateStr + ' ?')) {
    //   this.calendarEvents = this.calendarEvents.concat({ // add new event data. must create new array
    //     title: 'New Event',
    //     start: $event.date,
    //     allDay: $event.allDay
    //   });
    // }
  }

  handleEventClick($event: any) {
    console.log($event);
    this.popoverPosition = {
      // top: $event.el.getBoundingClientRect().top + document.scrollingElement.scrollTop + $event.el.getBoundingClientRect().height + 5,
      top: 10,
      left: $event.el.getBoundingClientRect().left + ($event.el.getBoundingClientRect().width / 4)
    };
    this.popoverClass = $event.event.classNames[0];
    this.popover.anchor = $event.el;
    // this.popover.position
    this.popover.open();
    // this.notificationMenuBtn.openMenu();
  }


}

