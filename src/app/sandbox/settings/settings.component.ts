import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ExportComponent} from './export/export.component';
import {ImportComponent} from './import/import.component';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  openExport() {
    const dialogRef = this.dialog.open(ExportComponent, {
      data: {},
      panelClass: 'main-popup',
      width: '750px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openImport() {
    const dialogRef = this.dialog.open(ImportComponent, {
      data: {},
      panelClass: 'main-popup',
      width: '750px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  constructor(public dialog: MatDialog) {
  }

  ngOnInit(): void {
  }

}
