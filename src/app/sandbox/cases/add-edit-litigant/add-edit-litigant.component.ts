import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {faTimes} from '@fortawesome/pro-light-svg-icons';

@Component({
  selector: 'app-add-edit-litigant',
  templateUrl: './add-edit-litigant.component.html',
  styleUrls: ['./add-edit-litigant.component.scss']
})
export class AddEditLitigantComponent implements OnInit {
  faTimes = faTimes;
  displayedColumns = ['select', 'Img', 'Name', 'ID', 'Title'];
  dataSource = new MatTableDataSource<ICaseRowData>(ELEMENT_DATA);
  selection = new SelectionModel<ICaseRowData>(true, []);

  constructor() {
  }

  ngOnInit(): void {
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
}

export interface ICaseRowData {
  Name: string;
  ID: string;
  Title: string;
  Img: string;
}

const ELEMENT_DATA: ICaseRowData[] = [
  {Name: 'Ahmed Alaa ELdin', Img: 'profile-img.png', ID: '5526', Title: 'Consultant'},
  {Name: 'Ahmed Alaa ELdin', Img: 'profile-img.png', ID: '5526', Title: 'Consultant'},
  {Name: 'Ahmed Alaa ELdin', Img: 'profile-img.png', ID: '5526', Title: 'Consultant'},
  {Name: 'Ahmed Alaa ELdin', Img: 'profile-img.png', ID: '5526', Title: 'Consultant'},
  {Name: 'Ahmed Alaa ELdin', Img: 'profile-img.png', ID: '5526', Title: 'Consultant'},
  {Name: 'Ahmed Alaa ELdin', Img: 'profile-img.png', ID: '5526', Title: 'Consultant'},
  {Name: 'Ahmed Alaa ELdin', Img: 'profile-img.png', ID: '5526', Title: 'Consultant'},
  {Name: 'Ahmed Alaa ELdin', Img: 'profile-img.png', ID: '5526', Title: 'Consultant'},
  {Name: 'Ahmed Alaa ELdin', Img: 'profile-img.png', ID: '5526', Title: 'Consultant'},
  {Name: 'Ahmed Alaa ELdin', Img: 'profile-img.png', ID: '5526', Title: 'Consultant'},
];
