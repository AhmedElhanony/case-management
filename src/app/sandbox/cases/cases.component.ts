import {AfterViewInit, Component, OnInit, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';
import {faSearch} from '@fortawesome/pro-regular-svg-icons';
import {CasesFiltersComponent} from './cases-filters/cases-filters.component';
import {MatDialog} from '@angular/material/dialog';
import {AddCasePopupComponent} from './add-case-popup/add-case-popup.component';
import {Router} from '@angular/router';
import {FeedBackComponent} from './feed-back/feed-back.component';
import {CompleteCaseInfoComponent} from './complete-case-info/complete-case-info.component';
import {ViewCasePopupComponent} from './view-case-popup/view-case-popup.component';

@Component({
  selector: 'app-cases',
  templateUrl: './cases.component.html',
  styleUrls: ['./cases.component.scss']
})
export class CasesComponent implements OnInit {
  faSearch = faSearch;
  displayedColumns = ['CaseName', 'CaseType', 'CaseOwner', 'Status', 'Actions'];
  dataSource = ELEMENT_DATA;

  constructor(public dialog: MatDialog, private route: Router) {
  }

  ngOnInit(): void {
  }


  addCasePopup() {
    const dialogRef = this.dialog.open(AddCasePopupComponent, {
      data: {},
      width: '1030px',
      panelClass: 'add-case-mobile-popup'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  completeInfo() {
    const dialogRef = this.dialog.open(CompleteCaseInfoComponent, {
      data: {},
      width: '1030px',
      panelClass: 'add-case-mobile-popup'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  viewCasePopup() {
    const dialogRef = this.dialog.open(ViewCasePopupComponent, {
      data: {},
      width: '1030px',
      panelClass: 'add-case-mobile-popup'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  feedBack() {
    const dialogRef = this.dialog.open(FeedBackComponent, {
      data: {},
      width: '600px',
      panelClass: 'main-popup'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openFilters() {
    const dialogRef = this.dialog.open(CasesFiltersComponent, {
      data: {},
      panelClass: 'mobile-popup',
      height: '60%'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}

export interface ICaseRowData {
  CaseName: string;
  CaseType: string;
  CaseOwner: string;
  Status: string;
}

const ELEMENT_DATA: ICaseRowData[] = [
  {CaseName: 'Case no.1 name', CaseType: 'Case Type no 1', CaseOwner: 'Mahmoud ali', Status: 'Sent back'},
  {CaseName: 'Case no.2 name', CaseType: 'Case Type no 1', CaseOwner: 'Mahmoud ali', Status: 'Waiting Approval'},
  {CaseName: 'Case no.3 name', CaseType: 'Case Type no 1', CaseOwner: 'Mahmoud ali', Status: 'Confirmed'},
  {CaseName: 'Case no.3 name', CaseType: 'Case Type no 1', CaseOwner: 'Mahmoud ali', Status: 'New'},
  {CaseName: 'Case no.1 name', CaseType: 'Case Type no 1', CaseOwner: 'Mahmoud ali', Status: 'Sent back'},
  {CaseName: 'Case no.2 name', CaseType: 'Case Type no 1', CaseOwner: 'Mahmoud ali', Status: 'Waiting Approval'},
  {CaseName: 'Case no.3 name', CaseType: 'Case Type no 1', CaseOwner: 'Mahmoud ali', Status: 'Confirmed'},
  {CaseName: 'Case no.3 name', CaseType: 'Case Type no 1', CaseOwner: 'Mahmoud ali', Status: 'New'},
  {CaseName: 'Case no.1 name', CaseType: 'Case Type no 1', CaseOwner: 'Mahmoud ali', Status: 'Sent back'},
  {CaseName: 'Case no.2 name', CaseType: 'Case Type no 1', CaseOwner: 'Mahmoud ali', Status: 'Waiting Approval'},
  {CaseName: 'Case no.3 name', CaseType: 'Case Type no 1', CaseOwner: 'Mahmoud ali', Status: 'Confirmed'},
  {CaseName: 'Case no.3 name', CaseType: 'Case Type no 1', CaseOwner: 'Mahmoud ali', Status: 'New'},
];
