import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewCasePopupComponent } from './view-case-popup.component';

describe('ViewCasePopupComponent', () => {
  let component: ViewCasePopupComponent;
  let fixture: ComponentFixture<ViewCasePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewCasePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewCasePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
