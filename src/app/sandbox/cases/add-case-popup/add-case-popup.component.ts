import {Component, OnInit, ViewChild} from '@angular/core';
import {faPlus, faTimes} from '@fortawesome/pro-light-svg-icons';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog} from '@angular/material/dialog';
import {OcrComponent} from '../ocr/ocr.component';

@Component({
  selector: 'app-add-case-popup',
  templateUrl: './add-case-popup.component.html',
  styleUrls: ['./add-case-popup.component.scss']
})
export class AddCasePopupComponent implements OnInit {
  @ViewChild('stepper', {static: true}) stepper;
  faTimes = faTimes;
  faPlus = faPlus;
  displayedColumns = ['select', 'Img', 'Name', 'ID', 'Title'];
  dataSource = new MatTableDataSource<ICaseRowData>(ELEMENT_DATA);
  selection = new SelectionModel<ICaseRowData>(true, []);

  constructor(public dialog: MatDialog) {
  }

  ngOnInit(): void {
  }

  prevHandler() {
    this.stepper.previous();
  }

  nextHandler() {

    this.stepper.next();

  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  animateStepperHeader() {
    /*todo:
    *  1- get translateX value of the stepper
    *  2- add the dx value to it
    *  3- consider the direction for every step
    * */
    const index = this.stepper.selectedIndex;
    const dx = 230;
    this.stepper.next();
    console.log(index);
    const stepperHeader = this.stepper._elementRef.nativeElement.querySelector('.mat-horizontal-stepper-header-container');
    stepperHeader.style.transform = `translateX(-${dx * index}px)`;
  }

  openOcr() {
    const dialogRef = this.dialog.open(OcrComponent, {
      data: {},
      panelClass: 'mobile-dialog',
      width: '984px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
}

export interface ICaseRowData {
  Name: string;
  ID: string;
  Title: string;
  Img: string;
}

const ELEMENT_DATA: ICaseRowData[] = [
  {Name: 'Ahmed Alaa ELdin', Img: 'profile-img.png', ID: '5526', Title: 'Consultant'},
  {Name: 'Ahmed Alaa ELdin', Img: 'profile-img.png', ID: '5526', Title: 'Consultant'},
  {Name: 'Ahmed Alaa ELdin', Img: 'profile-img.png', ID: '5526', Title: 'Consultant'},
  {Name: 'Ahmed Alaa ELdin', Img: 'profile-img.png', ID: '5526', Title: 'Consultant'},
  {Name: 'Ahmed Alaa ELdin', Img: 'profile-img.png', ID: '5526', Title: 'Consultant'},
  {Name: 'Ahmed Alaa ELdin', Img: 'profile-img.png', ID: '5526', Title: 'Consultant'},
  {Name: 'Ahmed Alaa ELdin', Img: 'profile-img.png', ID: '5526', Title: 'Consultant'},
  {Name: 'Ahmed Alaa ELdin', Img: 'profile-img.png', ID: '5526', Title: 'Consultant'},
  {Name: 'Ahmed Alaa ELdin', Img: 'profile-img.png', ID: '5526', Title: 'Consultant'},
  {Name: 'Ahmed Alaa ELdin', Img: 'profile-img.png', ID: '5526', Title: 'Consultant'},
];
