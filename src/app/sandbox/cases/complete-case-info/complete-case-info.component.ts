import { Component, OnInit } from '@angular/core';
import {faPlus, faTimes} from '@fortawesome/pro-light-svg-icons';

@Component({
  selector: 'app-complete-case-info',
  templateUrl: './complete-case-info.component.html',
  styleUrls: ['./complete-case-info.component.scss']
})
export class CompleteCaseInfoComponent implements OnInit {
  faPlus = faPlus;
  faTimes = faTimes;
  constructor() { }

  ngOnInit(): void {
  }

}
