import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompleteCaseInfoComponent } from './complete-case-info.component';

describe('CompleteCaseInfoComponent', () => {
  let component: CompleteCaseInfoComponent;
  let fixture: ComponentFixture<CompleteCaseInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompleteCaseInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompleteCaseInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
