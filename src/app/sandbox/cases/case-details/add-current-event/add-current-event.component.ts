import { Component, OnInit } from '@angular/core';
import {faPlus, faTimes} from '@fortawesome/pro-light-svg-icons';
import {MatDialog} from '@angular/material/dialog';
import {AddNextEventComponent} from '../add-next-event/add-next-event.component';
@Component({
  selector: 'app-add-current-event',
  templateUrl: './add-current-event.component.html',
  styleUrls: ['./add-current-event.component.scss']
})
export class AddCurrentEventComponent implements OnInit {
  faPlus = faPlus;
  faTimes = faTimes;
  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  openAddNextEvent() {
    const dialogRef = this.dialog.open(AddNextEventComponent, {
      data: {},
      panelClass: 'main-popup',
      width: '750px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
