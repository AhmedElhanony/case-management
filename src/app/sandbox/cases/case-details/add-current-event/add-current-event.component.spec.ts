import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCurrentEventComponent } from './add-current-event.component';

describe('AddCurrentEventComponent', () => {
  let component: AddCurrentEventComponent;
  let fixture: ComponentFixture<AddCurrentEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCurrentEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCurrentEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
