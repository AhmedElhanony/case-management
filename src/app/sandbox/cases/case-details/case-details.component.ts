import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {AddDocumentsComponent} from '../../content/add-documents/add-documents.component';
import {faEllipsisV} from '@fortawesome/free-solid-svg-icons';
import {CloseCasePopupComponent} from './close-case-popup/close-case-popup.component';
import {SendLawOfficeComponent} from './send-law-office/send-law-office.component';
import {LawOfficeRepliesComponent} from './law-office-replies/law-office-replies.component';
import {EditLitigantPopupComponent} from './edit-litigant-popup/edit-litigant-popup.component';
import {DelegateTeamMemberComponent} from './delegate-team-member/delegate-team-member.component';
import {FinalResultComponent} from './final-result/final-result.component';
import {InitialResultComponent} from './initial-result/initial-result.component';

@Component({
  selector: 'app-case-details',
  templateUrl: './case-details.component.html',
  styleUrls: ['./case-details.component.scss']
})
export class CaseDetailsComponent implements OnInit {
  faEllipsisV = faEllipsisV;
  readMore = false;

  constructor(public dialog: MatDialog) {
  }


  ngOnInit(): void {
  }

  openAddDoc() {
    const dialogRef = this.dialog.open(AddDocumentsComponent, {
      data: {},
      panelClass: 'mobile-dialog',
      width: '750px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openInitialResult() {
    const dialogRef = this.dialog.open(InitialResultComponent, {
      data: {},
      panelClass: 'main-popup',
      width: '750px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openFinalResult() {
    const dialogRef = this.dialog.open(FinalResultComponent, {
      data: {},
      panelClass: 'main-popup',
      width: '750px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }


  openSendToLawOfficePopup() {
    const dialogRef = this.dialog.open(SendLawOfficeComponent, {
      data: {},
      panelClass: 'mobile-dialog',
      width: '625px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openLawOfficeReplies() {
    const dialogRef = this.dialog.open(LawOfficeRepliesComponent, {
      data: {},
      panelClass: 'mobile-dialog',
      width: '625px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openEditLitigant() {
    const dialogRef = this.dialog.open(EditLitigantPopupComponent, {
      data: {},
      panelClass: 'mobile-dialog',
      width: '1030px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openDelegateTeamMember() {
    const dialogRef = this.dialog.open(DelegateTeamMemberComponent, {
      data: {},
      panelClass: 'mobile-dialog',
      width: '730px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  showMore() {
    this.readMore = !this.readMore;
  }
}
