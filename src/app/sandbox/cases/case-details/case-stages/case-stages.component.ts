import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {SatPopover} from '@ncstate/sat-popover';
import {AddEventComponent} from '../../../dashboard/upcoming-events/add-event/add-event.component';
import {MatDialog} from '@angular/material/dialog';
import {faPlus} from '@fortawesome/pro-light-svg-icons';
import {CaseNotePopupComponent} from '../case-note-popup/case-note-popup.component';
import {MatTabGroup} from '@angular/material/tabs';
import {AddCurrentEventComponent} from '../add-current-event/add-current-event.component';
import {AddNextEventComponent} from '../add-next-event/add-next-event.component';

@Component({
  selector: 'app-case-stages',
  templateUrl: './case-stages.component.html',
  styleUrls: ['./case-stages.component.scss']
})
export class CaseStagesComponent implements OnInit, AfterViewInit {
  faPlus = faPlus;
  @ViewChild(SatPopover, {static: true}) popover: SatPopover;
  @ViewChild('stageTabs', {static: true}) stageTabs;
  @ViewChild('stageName', {static: true}) stageName;
  @ViewChild('stageDueDate', {static: true}) stageDueDate;
  tabs = [
    {
      name: 'Creation date',
      date: '4 Nov 2019'
    },
    {
      name: 'test',
      date: '3 Dec 2019'
    }
  ];
  selected = new FormControl(0);
  autoFocus = true;
  restoreFocus = true;
  hasEvent = false;
  hasNextEvent = false;
  eventsList = [];
  // @Input('startEventList') startEventList: [] = [];
  notesList = [];
  private compiledTabs: NodeListOf<Element>;
  // @Output() newItemEvent = new EventEmitter();
  // openSideNave() {
  //   this.newItemEvent.emit();
  //   console.log('testttttttttttttttttttttt');
  // }
  // addNewItem(value: string) {
  //   this.newItemEvent.emit(value);
  // }
  constructor(public dialog: MatDialog) {

  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.compiledTabs = this.stageTabs._tabHeader._elementRef.nativeElement.querySelectorAll('.mat-tab-label');
    this.compiledTabs.forEach((tabElement: Element, index: number) => {
      tabElement.setAttribute('style', `z-index:${50 - index};`);
      if (index < 2) {
        tabElement.classList.add('mat-tab-label-completed');
      }
    });
  }

  addTab(stageName: string, stageDueDate: string) {
    this.tabs.push({
      name: stageName,
      date: stageDueDate
    });
    this.compiledTabs = this.stageTabs._tabHeader._elementRef.nativeElement.querySelectorAll('.mat-tab-label');
    this.compiledTabs.forEach((tabElement: Element, index: number) => {
      tabElement.setAttribute('style', `z-index:${50 - index};`);
    });
    this.selected.setValue(this.tabs.length - 1);
    this.popover.close();
    this.stageName.value = '';
    this.stageDueDate.value = '';
  }

  removeTab(index: number) {
    this.tabs.splice(index, 1);
  }

  openAddEvent() {
    const dialogRef = this.dialog.open(AddCurrentEventComponent, {
      data: {},
      panelClass: 'main-popup',
      width: '750px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.hasEvent = true;
      this.eventsList.push({
        name: 'Event name',
        date: 'Today - (12:00pm-1:00pm)'
      });
    });
  }

  openAddNextEvent() {
    const dialogRef = this.dialog.open(AddNextEventComponent, {
      data: {},
      panelClass: 'main-popup',
      width: '750px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.hasEvent = true;
      this.hasNextEvent = !this.hasNextEvent;
    });
  }

  openAddNote() {
    const dialogRef = this.dialog.open(CaseNotePopupComponent, {
      data: {},
      panelClass: 'mobile-dialog',
      width: '624px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.notesList.push({
        name: 'Note title',
        description: '- checklist one'
      });
    });
  }
}
