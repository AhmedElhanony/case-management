import { Component, OnInit } from '@angular/core';
import {faPlus, faTimes} from '@fortawesome/pro-light-svg-icons';
@Component({
  selector: 'app-final-result',
  templateUrl: './final-result.component.html',
  styleUrls: ['./final-result.component.scss']
})
export class FinalResultComponent implements OnInit {
  faPlus = faPlus;
  faTimes = faTimes;
  constructor() { }

  ngOnInit(): void {
  }

}
