import { Component, OnInit } from '@angular/core';
import {faPlus, faTimes} from '@fortawesome/pro-light-svg-icons';
@Component({
  selector: 'app-initial-result',
  templateUrl: './initial-result.component.html',
  styleUrls: ['./initial-result.component.scss']
})
export class InitialResultComponent implements OnInit {
  faPlus = faPlus;
  faTimes = faTimes;
  constructor() { }

  ngOnInit(): void {
  }

}
