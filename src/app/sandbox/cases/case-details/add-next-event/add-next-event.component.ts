import { Component, OnInit } from '@angular/core';
import {faPlus, faTimes} from '@fortawesome/pro-light-svg-icons';
@Component({
  selector: 'app-add-next-event',
  templateUrl: './add-next-event.component.html',
  styleUrls: ['./add-next-event.component.scss']
})
export class AddNextEventComponent implements OnInit {
  faPlus = faPlus;
  faTimes = faTimes;
  constructor() { }

  ngOnInit(): void {
  }

}
