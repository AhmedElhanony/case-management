import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNextEventComponent } from './add-next-event.component';

describe('AddNextEventComponent', () => {
  let component: AddNextEventComponent;
  let fixture: ComponentFixture<AddNextEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNextEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNextEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
