import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-filters',
  templateUrl: './dashboard-filters.component.html',
  styleUrls: ['./dashboard-filters.component.scss']
})
export class DashboardFiltersComponent implements OnInit {

  constructor() {}

  date: Date;

  ngOnInit() {

  }

  onValueChange(value: Date): void {
    this.date = value;
  }

}
