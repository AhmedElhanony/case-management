import {Component, OnInit} from '@angular/core';
import * as Highcharts from 'highcharts';
import {MatDialog} from '@angular/material/dialog';
import { DashboardFiltersComponent } from './dashboard-filters/dashboard-filters.component';
import { CasesDialogComponent } from './cases-dialog/cases-dialog.component';
import { AddEventComponent } from './upcoming-events/add-event/add-event.component';

declare var require: any;
const Boost = require('highcharts/modules/boost');
const noData = require('highcharts/modules/no-data-to-display');
const More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {

  constructor(public dialog: MatDialog) {
  }
  displayedColumns = ['CaseName', 'CaseNo', 'CaseOwner', 'CaseStage', 'Attorney', 'Status'];
  dataSource = ELEMENT_DATA;

  Highcharts: typeof Highcharts = Highcharts;
  caseChart: any = {
    chart: {
      type: 'pie',
      renderTo: 'container',
      width: 300,
      height: 300
    },
    colors: ['#058DC7', '#50B432'],
    title: {
      verticalAlign: 'middle',
      floating: true,
      text: '25%',
      y: 20,
      align: 'center',
      style: {
        fontWeight: '500',
        color: '#1D252D',
        fontSize: '50px',
        fontFamily: 'BetaSTC'
      }
    },
    // subtitle: {
    //   text: 'Closed',
    //   verticalAlign: 'middle',
    //   floating: true,
    //   y: 40,
    //   style: {
    //     fontWeight: '500',
    //     color: '#8E9AA0',
    //     fontSize: '13px',
    //     fontFamily: 'BetaSTC'
    //   }
    // },
    legend: {
      symbolHeight: .001,
      symbolWidth: .001,
      symbolRadius: .001,
      useHTML: true,
      labelFormatter() {
        // tslint:disable-next-line:max-line-length
        return '<div class="wrapper"><div class="square" style="background: ' + this.color + '"></div><div class="label">' + this.name + '</div></div>';
      }
    },
    tooltip: {
      shared: true,
      useHTML: true,
      headerFormat: '',
      // tslint:disable-next-line:max-line-length
      pointFormat: '<div class="tooltip-chart"><span class="square mx-1" style="display:inline-block;width:9px; height:9px; background-color: {point.color}"></span><span class="mx-1">{point.y}</span></div>'
    },
    plotOptions: {
      pie: {
        dataLabels: {
          enabled: false,

        },
        showInLegend: true,
        innerSize: '70%',
        startAngle: 0,
        colors: [
          '#4F008C',
          '#1BCED8',
          '#CA102F',
          '#02C389'
        ],
      }

    },

    series: [{
      data: [
        ['Open', 144],
        ['in progress ', 144],
        ['Delayed', 144],
        ['Closed / resolved', 144],
      ],
      type: 'pie'
    }],

  };


  taskChart: any = {
    chart: {
      type: 'pie',
      renderTo: 'container',
      width: 300,
      height: 300
    },
    colors: ['#058DC7', '#50B432'],
    title: {
      text: ''
    },
    subtitle: {
      text: ''
    },
    legend: {
      symbolHeight: .001,
      symbolWidth: .001,
      symbolRadius: .001,
      useHTML: true,
      labelFormatter() {
        // tslint:disable-next-line:max-line-length
        return '<div class="wrapper"><div class="square" style="background: ' + this.color + '"></div><div class="label">' + this.name + '</div></div>';
      }
    },
    tooltip: {
      shared: true,
      useHTML: true,
      headerFormat: '',
      // tslint:disable-next-line:max-line-length
      pointFormat: '<div class="tooltip-chart"><span class="square mx-1" style="display:inline-block;width:9px; height:9px; background-color: {point.color}"></span><span class="mx-1">{point.y}%</span></div>'
    },
    plotOptions: {
      pie: {
        dataLabels: {
          enabled: false,

        },
        showInLegend: true,
        startAngle: 0,
        colors: [
          '#02C389',
          '#1BCED8',
          '#CA102F'
        ],
      }

    },

    series: [{
      data: [
        ['Done', 144],
        ['in progress  ', 40],
        ['Due date', 10]
      ],
      type: 'pie'
    }],

  };

  classificationChart: any = {
    chart: {
      type: 'column',
      // height: 300,
      ignoreHiddenSeries: true,


    },
    title: {
      text: null
    },
    legend: {
      enabled: false
    },
    yAxis: {
      tickWidth: 1,
      tickColor: '#BBC8CE',
      startOnTick: true,
      endOnTick: true,
      lineWidth: 1,
      gridLineWidth: 0,
      lineColor: '#BBC8CE',

      title: {
        text: null
      },

      labels: {
        // step: 1,
        // format: '{value}%',

        overflow: 'justify',

        style: {
          color: '#1D252D',
          fontSize: '10px',
          fontWeight: 'light',
          fontFamily: 'BetaSTC'
        }
      }
    },
    xAxis: {
      categories: ['Financial', 'Criminal', 'Classification2', 'Classification3', 'Classification4', 'Classification5', 'Classification6'],
      title: {
        text: null
      },

      tickColor: '#BBC8CE',
      startOnTick: true,
      endOnTick: true,
      lineWidth: 1,
      gridLineWidth: 0,
      lineColor: '#BBC8CE',
      labels: {
        step: 1,

        style: {
          color: '#1D252D',
          fontSize: '10px',
          fontWeight: '500',
          fontFamily: 'BetaSTC'
        }
      },
    },
    tooltip: {
      useHTML: true,
      headerFormat: '',
      pointFormat:
        `<div class="tooltip-chart">
            <span class="square mx-1" style="display:inline-block;width:9px; height:9px; background-color: {point.color}">
            </span><span class="mx-1">{point.y}/{point.stackTotal}</span></div>`
    },
    plotOptions: {column: {stacking: 'normal', pointPadding: 0.2, borderWidth: 0, pointWidth: 20}},
    series: [
      {type: 'column', name: 'Done', color: '#02C389', data: [15, 13, 5, 6, 7, 12, 25]},
      {type: 'column', name: 'In Progress', color: '#FF375E', data: [51, 31, 12, 13, 20, 23, 2]}
    ]
  };


  openFilters() {
    const dialogRef = this.dialog.open(DashboardFiltersComponent, {
      data: {},
      panelClass: 'mobile-popup',
      height: '60%'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openCases() {
    const dialogRef = this.dialog.open(CasesDialogComponent, {
      data: {},
      panelClass: 'mobile-dialog',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openAddEvent() {
    const dialogRef = this.dialog.open(AddEventComponent, {
      data: {},
      panelClass: 'mobile-dialog',
      width: '60%'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  ngOnInit(): void {
  }

}



export interface ICaseRowData {
  CaseName: string;
  CaseNo: string;
  CaseOwner: string;
  CaseStage: string;
  Attorney: string;
  Status: string;
}

const ELEMENT_DATA: ICaseRowData[] = [
  {CaseName: 'Case no.1 name', CaseNo: '76432', CaseOwner: 'Mahmoud ali', CaseStage: 'Stage 1', Attorney: 'test', Status: 'test2'},
  {CaseName: 'Case no.2 name', CaseNo: '76432', CaseOwner: 'Mahmoud ali', CaseStage: 'Stage 1', Attorney: 'test', Status: 'test2'},
  {CaseName: 'Case no.3 name', CaseNo: '76432', CaseOwner: 'Mahmoud ali', CaseStage: 'Stage 1', Attorney: 'test', Status: 'test2'},
  {CaseName: 'Case no.4 name', CaseNo: '76432', CaseOwner: 'Mahmoud ali', CaseStage: 'Stage 1', Attorney: 'test', Status: 'test2'},
  {CaseName: 'Case no.5 name', CaseNo: '76432', CaseOwner: 'Mahmoud ali', CaseStage: 'Stage 1', Attorney: 'test', Status: 'test2'},
  {CaseName: 'Case no.6 name', CaseNo: '76432', CaseOwner: 'Mahmoud ali', CaseStage: 'Stage 1', Attorney: 'test', Status: 'test2'},
  {CaseName: 'Case no.7 name', CaseNo: '76432', CaseOwner: 'Mahmoud ali', CaseStage: 'Stage 1', Attorney: 'test', Status: 'test2'},
  {CaseName: 'Case no.8 name', CaseNo: '76432', CaseOwner: 'Mahmoud ali', CaseStage: 'Stage 1', Attorney: 'test', Status: 'test2'},
  {CaseName: 'Case no.9 name', CaseNo: '76432', CaseOwner: 'Mahmoud ali', CaseStage: 'Stage 1', Attorney: 'test', Status: 'test2'},
  {CaseName: 'Case no.1 name', CaseNo: '76432', CaseOwner: 'Mahmoud ali', CaseStage: 'Stage 1', Attorney: 'test', Status: 'test2'},
];
