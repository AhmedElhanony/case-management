import { Component, OnInit } from '@angular/core';
import {faPlus, faTimes} from '@fortawesome/pro-light-svg-icons';

@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.scss']
})
export class AddEventComponent implements OnInit {
  faPlus = faPlus;
  faTimes = faTimes;
  constructor() { }

  ngOnInit(): void {
  }

}
